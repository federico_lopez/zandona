@extends('pages.base')

@section('banner')
<div class="row text-center">
    <div class="col-12" data-aos="fade-in"  data-aos-easing="ease-in-out">
        <h2>Estética Dental</h2>
        <p data-aos="fade-in" data-aos-delay="600" data-aos-easing="ease-in-out"><a class="text-dark" href="{{ route('servicios.index') }}">Servicios</a></p>
    </div>
</div>
@endsection

@section('content')

<section id="periodoncia" class="my-5">
    <div class="container">
      <div class="row">
          <div class="col-md-8">
            <h3 class="font-weight-bold text-primary-dark">Periodoncia </h3>
            <img class="img-fluid my-3" src="{{ asset('images/servicios/periodoncia.jpg') }}" alt="Imagen de periodoncia">
            <p>Es la especialidad que permite diagnosticar y prevenir enfermedades en las encías, como la Gingivitis o la Periodontitis.</p>

            <h5>¿Qué es la Periodoncia?</h5>
            <p>La Periodoncia es una especialidad de la Odontología que estudia la <span class="font-weight-bold">prevención, diagnóstico y tratamiento</span> de las
                Enfermedades Periodontales, es decir, aquellas que son provocadas por la acumulación de placa bacteriana que posteriormente al endurecerse se convertirá en
                sarro en los dientes y por debajo de la encía.</p>
            <p>Dentro de las Enfermedades Periodontales más comunes que la Periodoncia alivia se encuentran la Gingivitis y Periodontitis; las cuales provocan mal aliento e
                incluso pueden ocasionar la pérdida de las piezas dentales.</p>

            <h5>¿Qué es la Gingivitis?</h5>
            <p>La Gingivitis es una enfermedad inflamatoria causada por una higiene oral inadecuada, es decir, por las bacterias de la placa dentobacteriana
                (una película pegajosa e incolora) que de no ser eliminada durante el cepillado diario, comienza a acumularse en la superficie dental. </p>
            <p>A menudo los pacientes desconocen qué es la Gingivitis o cuáles son sus síntomas, sin embargo, esta enfermedad es silenciosa y no causa dolor.
                Se identifica cuando el color de la encía cambia a un rojo intenso, se inflama y se presenta sangrado durante el cepillado.</p>

            <h5>¿Cómo aparece la Periodontitis?</h5>
            <p>Cuando no se trata una Gingivitis, aparece la Periodontitis, inflamación grave en las encías que provoca mal aliento y una infección que
                llega hasta los ligamentos y el hueso, los cuales sirven de soporte a los dientes, causando que éstos se aflojen y finalmente se caigan.</p>

            <h5>¿Cómo ayuda la Periodoncia?</h5>
            <p>Para tratar las Enfermedades Periodontales, el médico realiza una limpieza profunda en la cual se elimina la placa bacteriana y el sarro.
                El objetivo del tratamiento es reducir la inflamación y eliminar las bolsas en las encías.</p>
            <p>Sin embargo, si la enfermedad está avanzada podría ser necesario practicar una Cirugía Periodontal para conseguir este objetivo y, de ser necesario,
                utilizar técnicas para regenerar el hueso que se haya perdido</p>
            <p>También se enseña al paciente la importancia de los cuidados orales para que no vuelva a aparecer ningún tipo de infección.</p>

            <h5>Beneficios de la Periodoncia</h5>
            <p>Si ya conoces qué es la Periodoncia, ahora te mencionamos sus principales beneficios:</p>
            <ul>
                <li>Recuperar la salud de las encías.</li>
                <li>Eliminar la placa bacteriana.</li>
                <li>Realizar limpieza dental profunda para prevenir la futura aparición de Periodontitis.</li>
                <li>Frenar la pérdida de huesocausada por la Periodontitis.</li>
                <li>Eliminar el mal aliento.</li>
                <li>Recuperar la estética de las encías.</li>
            </ul>
            <p>En general para evitar recurrir a este tipo de tratamiento dental y mantener una sonrisa sana, las personas deberían acudir regularmente a una clínica
                dental y realizar una limpieza cada 6 meses.</p>
            <p>Si no es así, es fácil que la Gingivitis progrese y se convierta en Periodontitis, con la acumulación de placa bacteriana y sarro. En consecuencia, dañar
                la estructura ósea y tejidos que lo sostienen, presentándose movilidad dental.</p>
            <p>Ahora que conoces qué es la Periodoncia, sabrás que la mejor forma de prevenir las Enfermedades Periodontales es realizar el cepillado 3 veces al día,
                uso de enjuague bucal e hilo dental, así como visitas periódicas con el especialista. </p>
            <p>Contamos con un Especialista en Periodoncia, quien te brindarán un diagnóstico y tratamientos adecuados para obtener una sonrisa sana. </p>

          </div>
          <div class="col-md-4 pt-md-0 pt-4">
            <h5>Otros Services</h5>
            <ul class="list-group">
                <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'estetica']) }}">Estética Dental</a></li>
                <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'implantes']) }}">Implantes</a></li>
                <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'ortodoncia']) }}">Ortodoncia</a></li>
                <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'endodoncia']) }}">Endodoncia</a></li>
                <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'odontopediatria']) }}">Odontopediatría</a></li>
            </ul>
          </div>
      </div>
    </div>
  </section>

@endsection



@section('script')
<script>
    $('#banner').addClass("background-servicios background-banner");
</script>
@endsection
