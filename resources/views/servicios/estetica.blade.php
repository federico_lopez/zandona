@extends('pages.base')

@section('banner')
<div class="row text-center">
    <div class="col-12" data-aos="fade-in"  data-aos-easing="ease-in-out">
        <h2>Estética Dental</h2>
        <p data-aos="fade-in" data-aos-delay="600" data-aos-easing="ease-in-out"><a class="text-dark" href="{{ route('servicios.index') }}">Servicios</a></p>
    </div>
</div>
@endsection

@section('content')

<section id="estetica-dental" class="my-5">
    <div class="container">
      <div class="row">
          <div class="col-md-8">
            <h3 class="font-weight-bold text-primary-dark">Estética Dental</h3>
              <img class="img-fluid my-3" src="{{ asset('images/servicios/estetica-dental.jpg') }}" alt="Imagen de estetica dental">
            <p>La Estética y Cósmetica Dental es un conjunto de tratamientos orientados al Diseño Personalizado de la Sonrisa. A través de un analisis facial, estudiaremos sus facciones, medidas y proporciones. La misma puede ayudar a la confianza y autoestima consiguiendo que sonrias más.</p>
            <p>La Estética Dental debe complementar la Salud Oral del paciente.</p>
            <p>La finalidad  de la Estética Dental es mejorar <span class="font-weight-bold">la sonrisa, calidad de vida, la apariencia y función</span> de los dientes con procedimientos clínicos como enderezar, aclarar, reformar y reparar la estética blanca (dientes) y la estética rosada (encia).</p>
            <p>Los distintos procedimientos en Estética Dental para un Diseño de Sonrisa son:<br></p>
            <ul>
              <li>Cirugia Plástica de la Encía (Gingivoplastia).</li>
              <li>Blanqueamiento Dental.</li>
              <li>Remodelado de Contorno Dental.</li>
              <li>Carillas Dentales.</li>
              <li>Lentes de Contactos Dentales.</li>
              <li>Coronas Dentales.</li>
            </ul>
          </div>
          <div class="col-md-4 pt-md-0 pt-4">
            <h5>Otros Services</h5>
            <ul class="list-group">
                <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'implantes']) }}">Implantes</a></li>
                <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'ortodoncia']) }}">Ortodoncia</a></li>
                <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'endodoncia']) }}">Endodoncia</a></li>
                <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'periodoncia']) }}">Periodoncia</a></li>
                <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'odontopediatria']) }}">Odontopediatría</a></li>
            </ul>
          </div>
      </div>
    </div>
  </section>

@endsection



@section('script')
<script>
    $('#banner').addClass("background-servicios background-banner");
</script>
@endsection
