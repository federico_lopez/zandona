@extends('pages.base')

@section('banner')
<div class="row text-center">
    <div class="col-12" data-aos="fade-in"  data-aos-easing="ease-in-out">
        <h2>Implantes</h2>
        <p data-aos="fade-in" data-aos-delay="600" data-aos-easing="ease-in-out"><a class="text-dark" href="{{ route('servicios.index') }}">Servicios</a></p>
    </div>
</div>
@endsection

@section('content')

<section id="implantes" class="my-5">
    <div class="container">
      <div class="row">
          <div class="col-md-8">
                <h3 class="font-weight-bold text-primary-dark">Implantes</h3>
                <img class="img-fluid my-3" src="{{ asset('images/servicios/implantes.jpg') }}" alt="Imagen de implante dental">
                <p>La Implantología Oral es la rama de la Odontología que se ocupa de reemplazar los dientes perdidos por distintas causas, valiéndose de Implantes Dentales
                    colocados en el hueso maxilar o mandibular. Estas prótesis, de diferentes tamaños y materiales, permiten la restitución de las funciones
                    <span class="font-weight-bold">masticatorias, fonéticas y estéticas</span>, recuperando la correcta estructura y salud bucal. Un Implante Dental es
                    básicamente un tornillo de Titanio puro que, gracias a la biocompatibilidad lograda a través de un tratamiento especial al que ha sido sometido, tiene
                    la facultad de integrarse perfectamente a los componentes óseos correspondientes. La aplicación se completa con un conector y la funda o corona.
                    La Implantología Bucal tiene también como objetivo el estudio de nuevos materiales que garanticen una adecuada adaptación del elemento sustitutivo al organismo.</p>
                <h5>¿Qué Odontólogos la practican?</h5>
                <p>Los Implantólogos son los profesionales acreditados para desarrollar esta Especialidad Médica que requiere de un programa de estudio de posgrado en el área
                    específica. Esta práctica es abordada frecuentemente de manera interdisciplinaria, interviniendo Odontólogos generales, Cirujanos y Prostodoncistas en
                    el análisis y planificación de las distintas fases del tratamiento. Así, el equipo determina a través del diagnóstico y pronóstico del caso los métodos
                    y tipos de prótesis más convenientes para asegurar el éxito del procedimiento.</p>
                <h5>¿Cuándo es necesario un Implante Dental?</h5>
                <p>Existen diferentes situaciones en las cuales es indicada la colocación de Implantes Bucales. Es requerida la aplicación de una prótesis cuando
                    ciertas enfermedades como caries, piorrea o periodontitis afectan la dentadura de jóvenes o mayores, provocando la pérdida de una pieza dentaria.
                    En algunas ocasiones, y debido a factores congénitos, hay dientes que nunca completan su desarrollo total por lo que la Implantología Oral es el recurso
                    idóneo para resolver el problema. Es también frecuente la pérdida de elementos dentales como producto de diversos accidentes y en estos casos la solución
                    correcta es recurrir a la aplicación de Implantes. La secuencia básica de la práctica Implantológica en el consultorio, cualquiera sea el motivo de su
                    utilización, consta de tres fases: Planificación, Intervención Quirúrgica y Colocación de la Prótesis. Los Odontólogos destacan que probablemente los
                    Implantes Dentales representen el único caso en que un sustituto artificial logre una funcionalidad más efectiva que el órgano original y que,
                    trabajando eficazmente en la boca, alcanzan una vida útil superior a la de una pieza natural.</p>
                <h5>¿A partir de qué edad se efectúan?</h5>
                <p>La colocación de Implantes Dentales puede ser efectuada en jóvenes que ya han completado su desarrollo. En líneas generales, puede estimarse que
                    las mujeres a partir de los dieciséis años y los varones una vez cumplidos los dieciocho se encuentran en condiciones de ser sometidos a este tratamiento.
                    En cuanto a las personas adultas, la edad no supone una restricción en ningún caso. En general no existen contraindicaciones que desaconsejen la aplicación
                    de Implantes, con excepción de enfermedades graves como ciertas infecciones y tumores. Al tratarse de una intervención ambulatoria que no requiere de
                    internación, la recuperación del paciente es rápida y no implica un período posterior a la operación complicado.</p>
            </div>
         <div class="col-md-4 pt-md-0 pt-4">
            <h5>Otros Services</h5>
            <ul class="list-group">
                <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'estetica']) }}">Estética Dental</a></li>
                <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'ortodoncia']) }}">Ortodoncia</a></li>
                <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'endodoncia']) }}">Endodoncia</a></li>
                <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'periodoncia']) }}">Periodoncia</a></li>
                <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'odontopediatria']) }}">Odontopediatría</a></li>
            </ul>
          </div>
      </div>
    </div>
  </section>

@endsection


@section('script')
<script>$('#banner').addClass("background-servicios background-banner");</script>
@endsection
