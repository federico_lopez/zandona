@extends('pages.base')

@section('banner')
<div class="row text-center">
    <div class="col-12" data-aos="fade-in"  data-aos-easing="ease-in-out">
        <h2>Estética Dental</h2>
        <p data-aos="fade-in" data-aos-delay="600" data-aos-easing="ease-in-out"><a class="text-dark" href="{{ route('servicios.index') }}">Servicios</a></p>
    </div>
</div>
@endsection

@section('content')

<section id="endodoncia" class="my-5">
    <div class="container">
      <div class="row">
            <div class="col-md-8">
                <h3 class="font-weight-bold text-primary-dark">Endodoncia</h3>
                <img class="img-fluid my-3" src="{{ asset('images/servicios/endodoncia.jpg') }}" alt="Imagen de endodoncia">
                <p>La Endodoncia es la especialidad de la odontología que comprende la <span class="font-weight-bold">prevención, el diagnóstico y el tratamiento</span> de las patologías de la pulpa dental y de los tejidos peri radiculares.</p>
                <p>La pulpa dental es el tejido localizado en el interior de la cavidad del diente y se compone de cámara pulpar y conducto radicular.</p>
                <p>El Tratamiento Endodóntico se conoce normalmente con el nombre &quot;Tratamiento de Conducto&quot;.</p>

                <h5>Las Causas</h5>
                <p>La caries dental es la causa más común de la inflamación pulpar. Entre otras causas y factores que pueden llevar a la necesidad de un Tratamiento Endodóntico podemos citar los traumatismos dentarios, inflamación de la pulpa ocasionada por la infección periodontal, necesidades protésicas, abrasiones, entre otras.</p>

                <h5>Los síntomas más característicos</h5>
                <p>Dolor espontáneo, en forma de latido, que aumenta con el frío o el calor. En ese caso la pulpa está aún con vitalidad, pero con inflamación, y el uso de
                    analgésicos no resulta en la remisión de los síntomas. Cuando la pulpa pierde su vitalidad puede producirse acumulación de pus en el hueso que rodea la
                    raíz (absceso) generalmente el dolor es bien localizado y aumenta al masticar. Otros síntomas pueden acompañar en cada caso particular.</p>

                <h5>El tratamiento</h5>
                <p>Cuando la pulpa dentaria se encuentra inflamada irreversiblemente, necrosada o infectada es necesario realizar el Tratamiento Endodóntico. El Tratamiento Endodóntico
                    consiste en la remoción de la pulpa dental enferma, modelado de los conductos y obturación del espacio antes ocupado por la pulpa.</p>
                <p>Después que el Tratamiento Endodóntico se concluye, el diente necesitará de una restauración o prótesis para volver a su forma y funciones normales.
                    Es muy importante el control radiográfico que debe ser realizado después de seis meses de concluido el Tratamiento Endodóntico para evaluar el resultado
                    del procedimiento.</p>

                <h5>Ventajas</h5>
                <p>Salvar una pieza dentaria mediante un Tratamiento Endodóntico tiene muchas ventajas:</p>
                <ul>
                    <li>Masticación eficiente</li>
                    <li>Fuerzas normales de masticación y la sensibilidad</li>
                    <li>Apariencia natural</li>
                    <li>Protege a los otros dientes de las fuerzas excesivas</li>
                </ul>

                <h5>¿Es un tratamiento doloroso?</h5>
                <p>Con el uso de anestesia local el tratamiento es completamente indoloro.</p>

                <h5>¿Siempre se oscurece el diente después del Tratamiento Endodóntico?</h5>
                <p>El oscurecimiento acentuado es más frecuente cuando se produjo hemorragia o mortificación pulpar antes del tratamiento.
                    Sin embargo aún en esos casos puede devolverse el color original con procedimientos de estética dental.</p>

                <h5>¿Que puede ocurrir si no se realiza el Tratamiento Endodóntico?</h5>
                <p>Se puede desarrollar una lesión periapical (infección del hueso que rodea y sostiene a la raíz y de los tejidos vecinos), que puede tener consecuencias
                    más serias, como dolor intenso, hinchazón, fiebre y bacteriemia (bacterias en la circulación sanguínea). La solución en esos casos puede comprender
                    la extracción del diente.</p>
          </div>

          <div class="col-md-4 pt-md-0 pt-4">
            <h5>Otros Services</h5>
            <ul class="list-group">
                <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'estetica']) }}">Estética Dental</a></li>
                <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'implantes']) }}">Implantes</a></li>
                <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'ortodoncia']) }}">Ortodoncia</a></li>
                <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'periodoncia']) }}">Periodoncia</a></li>
                <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'odontopediatria']) }}">Odontopediatría</a></li>
            </ul>
          </div>
      </div>
    </div>
  </section>

@endsection



@section('script')
<script>
    $('#banner').addClass("background-servicios background-banner");
</script>
@endsection
