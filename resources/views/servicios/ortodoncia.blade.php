@extends('pages.base')

@section('banner')
<div class="row text-center">
    <div class="col-12" data-aos="fade-in"  data-aos-easing="ease-in-out">
        <h2>Estética Dental</h2>
        <p data-aos="fade-in" data-aos-delay="600" data-aos-easing="ease-in-out"><a class="text-dark" href="{{ route('servicios.index') }}">Servicios</a></p>
    </div>
</div>
@endsection

@section('content')

<section id="ortodoncia" class="my-5">
    <div class="container">
      <div class="row">
          <div class="col-md-8">
            <h3 class="font-weight-bold text-primary-dark">Ortodoncia</h3>
            <img class="img-fluid my-3" src="{{ asset('images/servicios/ortodoncia.png') }}" alt="Imagen de ortodoncia">
            <p>
                Es mucho más que aquellos problemas únicamente estéticos, la boca es una parte fundamental del buen funcionamiento del organismo.
                Ortodoncia es una especialidad de la Odontología que se encarga de la corrección de <span class="font-weight-bold">los dientes y huesos</span> posicionados
                incorrectamente.
            </p>
            <p>Tienen la ventaja de proporcionarnos una boca sana, una sonrisa de aspecto agradable y dientes con mayores posibilidades de durar toda la vida.</p>
            <p>En niños, lo normal es comenzar con tratamientos interceptivos que son los que evitan y previenen alteraciones mayores.</p>
            <p>La edad adecuada para tratar las maloclusiones varía según el tipo de problema y su gravedad. Por eso es aconsejable consultar al especialista tan pronto como se detecte el problema.</p>
            <p>Aunque este tipo de tratamientos se asocia a niños y adolescentes, no significa que sea un tratamiento exclusivo para ellos. La Ortodoncia en adultos es cada vez más común, con muchas personas tratandose con éxito.</p>

          </div>
          <div class="col-md-4 pt-md-0 pt-4">
            <h5>Otros Services</h5>
            <ul class="list-group">
                <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'estetica']) }}">Estética Dental</a></li>
                <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'implantes']) }}">Implantes</a></li>
                <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'endodoncia']) }}">Endodoncia</a></li>
                <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'periodoncia']) }}">Periodoncia</a></li>
                <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'odontopediatria']) }}">Odontopediatría</a></li>
            </ul>
          </div>
      </div>
    </div>
  </section>

@endsection



@section('script')
<script>
    $('#banner').addClass("background-servicios background-banner");
</script>
@endsection


