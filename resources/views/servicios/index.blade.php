@extends('pages.base')

@section('banner')
<div class="row">
    <div class="col-12" data-aos="fade-in"  data-aos-easing="ease-in-out">
        <h2 class="text-light">Servicios</h2>
    </div>
</div>
@endsection

@section('content')

<section>
    <div class="container">
        <div class="row pt-5 pb-3">
            <div class="col-sm-8 mx-auto text-center">
                <h2 class="text-primary">Nuestros Servicios</h2>
                <p>Te ofrecemos servicios diferenciados para cada tipo de pacientes.</p>
            </div>
        </div>
    </div>
</section>

<section class="mb-5 pb-3">
    <div class="container">
        <div class="row">
            <div class="col-md-4 mb-4">
                <div class="card">
                    <img class="card-img-top" src="{{ asset('images/servicios/estetica-dental.jpg') }}" alt="imagen de estetica dental">
                    <div class="card-body">
                      <h3 class="card-title">Estética Dental</h3>
                      <p class="card-text text-muted">La Estética y Cósmetica Dental es un conjunto de tratamientos orientados al Diseño Personalizado de la Sonrisa.</p>
                    <a href="{{ route('servicios.url', ['url' => 'estetica']) }}" class="btn btn-outline-dark btn-block">Ver más</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-4">
                <div class="card">
                    <img class="card-img-top" src="{{ asset('images/servicios/implantes.jpg') }}" alt="imagen de implantes">
                    <div class="card-body">
                      <h3 class="card-title">Implantes</h3>
                      <p class="card-text text-muted">La implantología oral es la rama de la odontología que se ocupa de reemplazar los dientes perdidos por distintas causas.</p>
                    <a href="{{ route('servicios.url', ['url' => 'implantes']) }}" class="btn btn-outline-dark btn-block">Ver más</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-4">
                <div class="card">
                    <img class="card-img-top" src="{{ asset('images/servicios/ortodoncia.png') }}" alt="imagen de ortodoncia">
                    <div class="card-body">
                      <h3 class="card-title">Ortodoncia</h3>
                      <p class="card-text text-muted">Es una especialidad de la odontología que se encarga de la corrección de los dientes y huesos posicionados incorrectamente.</p>
                    <a href="{{ route('servicios.url', ['url' => 'ortodoncia']) }}" class="btn btn-outline-dark btn-block">Ver más</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-4">
                <div class="card">
                    <img class="card-img-top" src="{{ asset('images/servicios/endodoncia.jpg') }}" alt="imagen de endodoncia">
                    <div class="card-body">
                      <h3 class="card-title">Endodoncia</h3>
                      <p class="card-text text-muted">Es la especialidad que comprende la prevención, el diagnóstico y el tratamiento de las patologías de la pulpa dental y de los tejidos peri radiculares.</p>
                    <a href="{{ route('servicios.url', ['url' => 'endodoncia']) }}" class="btn btn-outline-dark btn-block">Ver más</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-4">
                <div class="card">
                    <img class="card-img-top" src="{{ asset('images/servicios/periodoncia.jpg') }}" alt="imagen de periodoncia">
                    <div class="card-body">
                      <h3 class="card-title">Periodoncia</h3>
                      <p class="card-text text-muted">Es la especialidad que permite diagnosticar y prevenir enfermedades en las encías, como la gingivitis o la periodontitis.</p>
                    <a href="{{ route('servicios.url', ['url' => 'periodoncia']) }}" class="btn btn-outline-dark btn-block">Ver más</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-4">
                <div class="card">
                    <img class="card-img-top" src="{{ asset('images/servicios/odontopediatria.jpg') }}" alt="imagen de odontopediatria">
                    <div class="card-body">
                      <h3 class="card-title">Odontopediatría</h3>
                      <p class="card-text text-muted">Esta disciplina está encargada de explorar a los recién nacidos, niños y pre adolescentes.</p>
                    <a href="{{ route('servicios.url', ['url' => 'odontopediatria']) }}" class="btn btn-outline-dark btn-block">Ver más</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>








@endsection



@section('script')
<script>
    $('#banner').addClass("background-servicios background-banner");
</script>
@endsection
