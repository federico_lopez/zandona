@extends('pages.base')

@section('banner')
<div class="row text-center">
    <div class="col-12" data-aos="fade-in"  data-aos-easing="ease-in-out">
        <h2>Implantes</h2>
        <p data-aos="fade-in" data-aos-delay="600" data-aos-easing="ease-in-out"><a class="text-dark" href="{{ route('servicios.index') }}">Servicios</a></p>
    </div>
</div>
@endsection

@section('content')

<section id="odontopediatria" class="my-5">
    <div class="container">
      <div class="row">
          <div class="col-md-8">
            <h3 class="font-weight-bold text-primary-dark">Odontopediatría</h3>
            <img class="img-fluid my-3" src="{{ asset('images/servicios/odontopediatria.jpg') }}" alt="Imagen de odontopediatria">
            <p>Esta disciplina está encargada de explorar a los <span class="font-weight-bold">recién nacidos, niños y pre adolescentes</span>. Ademas, también se ocupa de descubrir posibles anomalías en el lugar de los dientes o maxilares. </p>
            <p>La odontopediatría sólo trata a niños que aún tengan dientes de leche.</p>
            <p>Cuando un odontólogo asume la responsabilidad de trabajar con niños, el objetivo principal del odontopediatra es el cuidado sicológico (grata experiencia de las primeras consultas), oral preventivo y terapéutico.</p>
            </div>
         <div class="col-md-4 pt-md-0 pt-4">
            <h5>Otros Services</h5>
            <ul class="list-group">
                <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'estetica']) }}">Estética Dental</a></li>
                <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'implantes']) }}">Implantes</a></li>
                <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'ortodoncia']) }}">Ortodoncia</a></li>
                <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'endodoncia']) }}">Endodoncia</a></li>
                <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'periodoncia']) }}">Periodoncia</a></li>
            </ul>
          </div>
      </div>
    </div>
  </section>

@endsection


@section('script')
<script>$('#banner').addClass("background-servicios background-banner");</script>
@endsection


<section id="odontopediatria" class="mt-2">
    <div class="container">
        <div class="row">
            <div class="col-12">
                </div>
        </div>
    </div>
</section>
