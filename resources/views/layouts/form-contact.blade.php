 <form method="POST" action="{{ route('pages.mail') }}" novalidate>
{{--<form novalidate>--}}
    @csrf

    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="inputEmail4">Nombre</label>
            <input type="text" class="form-control @error('nombre') is-invalid @enderror" id="nombre" name="nombre" value="{{ old('nombre') }}">
            @error('nombre')
            <span class="text-danger">
                <strong>Error: {{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="form-group col-md-6">
            <label for="email">Whatsapp o email</label>
            <input type="text" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email') }}">
            @error('email')
            <span class="text-danger">
                <strong>Error: {{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="servicio">Servicio</label>
            <input type="text" class="form-control @error('email') is-invalid @enderror" id="servicio" name="servicio"  value="{{ old('servicio') }}">
            @error('servicio')
            <span class="text-danger">
                <strong>Error: {{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="form-group col-md-6">
            <label for="doctor">Doctor de Preferencia</label>
            <select id="doctor" class="form-control @error('doctor') is-invalid @enderror" name="doctor">
                <option value="">Seleccione un Dr.</option>
                <option value="Dra. Sandra Zandona" {{ old('doctor') == 'Dra. Sandra Zandona' ? 'selected' : '' }}>Dra. Sandra Zandona</option>
                <option value="Dr. Miguel Riquelme Rodas" {{ old('doctor') == 'Dr. Miguel Riquelme Rodas' ? 'selected' : '' }}>Dr. Miguel Riquelme Rodas</option>
                <option value="Dra. Silvia Zelada" {{ old('doctor') == 'Dra. Silvia Zelada' ? 'selected' : '' }}>Dra. Silvia Zelada</option>
                <option value="Dra. Noelia Olmedo" {{ old('doctor') == 'Dra. Noelia Olmedo' ? 'selected' : '' }}>Dra. Noelia Olmedo</option>
                <option value="Dr. Carlos Heilborn" {{ old('doctor') == 'Dr. Carlos Heilborn' ? 'selected' : '' }}>Dr. Carlos Heilborn</option>
                <option value="No tengo preferencia" {{ old('doctor') == 'No tengo preferencia' ? 'selected' : '' }}>No tengo preferncia</option>
            </select>
            @error('doctor')
            <span class="text-danger">
                <strong>Error: {{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <div class="form-group">
        <label for="detalle">Detalle del servicio que desea realizarse</label>
        <textarea class="form-control @error('mensaje') is-invalid @enderror" name="mensaje" rows="3" id="mensaje">{{ old('mensaje') }}</textarea>
        @error('mensaje')
            <span class="text-danger">
                <strong>Error: {{ $message }}</strong>
            </span>
        @enderror
    </div>

    <button type="submit" class="btn btn-primary">Enviar</button>
</form>
