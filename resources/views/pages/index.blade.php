@extends('pages.base')

@section('banner')
<div class="row pb-5 align-items-center">
    <div class="col-md-6">
        <h1>EXPERTOS EN SONRISAS</h1>
        <p class="text-light py-2 mb-3">Nos inspira la Belleza. La belleza entendida como la armonía Estética y Ética que se logra combinando <span class="font-weight-bold">ciencia, experiencia</span>  y, sobre todo, <span class="font-weight-bold">pasión</span>. Pasión por brindar la mejor experiencia médica a cada uno de nuestros pacientes. A través de nuestras premisas: <span class="font-weight-bold">educar, prevenir, tratar y mantener</span>, ofrecemos  una experiencia de cuidado integral guiada por los mejores estándares médicos y de atención de la salud.</p>
        {{-- <button type="button" class="btn btn-primary btn-lg text-white px-5" data-toggle="modal" data-target="#contactanos"  data-aos="fade-up" data-aos-duration="900" data-aos-easing="ease-in-out">Contáctanos</button> --}}
        <a href="{{ route('pages.contacto') }}" class="btn btn-primary btn-lg text-white px-5"  data-aos="fade-up" data-aos-duration="900" data-aos-easing="ease-in-out">Contáctanos</a>
    </div>
    <div class="col-md-6">
        <img src="{{ asset('images/index/perla.jpg') }}" alt="perla" class="img-fluid" data-aos="fade-in" data-aos-delay="700" data-aos-easing="ease-in-out">
    </div>
</div>
@endsection

@section('content')

<section id="cuidado-bucal">
    <div class="container p-4 p-md-0">
        <div class="row py-3 py-md-5 align-items-center">
            <div class="col-12 col-md-10 col-lg-6 offset-lg-6">
                <h2 class="text-primary">La mejor experiencia de cuidado de su salud bucal</h2>
                <p class="pt-3">Nuestra visión integral de cuidado inicia con la educación para la prevención.<br>Guiamos a nuestros pacientes en una experiencia que arranca con la información para el cuidado bucal. No solo queremos tratar, sino también prevenir las afecciones y mantener una buena salud bucal.</p>
            </div>
        </div>
    </div>
</section>

<section id="historia">
    <div class="container pl-4">
        <div class="row pt-5">
            <div class="col-12 col-sm-8 offset-sm-4 col-md-6 offset-md-6">
                <h2 class="text-uppercase">La historia de un sueño</h2>
                <p>Me gustaría compartirles la historia que dio inicio a este sueño pero que comenzó a gestarse mucho tiempo antes...</p>
                <a class="btn btn-outline-light text-uppercase btn-block" href="{{ route('pages.historia') }}" data-aos="fade-right" data-aos-duration="1000">leer más</a>
            </div>
        </div>
    </div>
</section>

  <section id="conozcanos">
    <div class="container pl-4">
        <div class="row py-5">
            <div class="col-12 col-sm-9 col-md-6 col-lg-5">
                <h2 class="text-primary">Conózcanos</h2>
                <p class="text-white">La experiencia Zandoná nace del compromiso con los detalles. Aquellos que hacen de su tratamiento una experiencia única de cuidado integral de su salud bucal.</p>
                <p class="text-white">Además de acceder a una Clínica con tecnología de punta, los mejores Profesionales expertos en su materia, nuestros pacientes viven una experiencia de cuidado exclusivo que va desde la atención al tiempo (nuestros pacientes no esperan), al espacio (queremos que se sientan cómodos) y las formas (queremos ofrecerles solamente lo mejor).</p>
                <ul class="list-group list-group-flush" data-aos="fade-right"  data-aos-delay="200">
                    <li class="list-group-item"><a href="{{ route('pages.mision') }}">Misión</a></li>
                    <li class="list-group-item"><a href="{{ route('pages.profesionales') }}">Profesionales</a></li>
                    <li class="list-group-item"><a href="{{ route('pages.clinica') }}">La Clínica</a></li>
                </ul>
            </div>
        </div>
    </div>
  </section>


    <section id="servicios">
        <div class="opacitys">
            <div class="container pl-4">
                <div class="row py-5">
                    <div class="col-12 col-sm-9 col-md-6">
                        <h2>Servicios</h2>
                        <p>Nuestros pacientes tienen desde la primera consulta una introduccion a los cuidados fundamentales de higiene bucal y un acompañamiento en el postratamiento, que para nosotros, es el pilar esencial del éxito sostenible a lo largo del tiempo.</p>
                        <ul class="list-group list-group-flush" data-aos="fade-right"  data-aos-delay="200">
                            <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'estetica']) }}">Estética Dental</a></li>
                            <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'implantes']) }}">Implantes</a></li>
                            <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'ortodoncia']) }}">Ortodoncia</a></li>
                            <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'endodoncia']) }}">Endodoncia</a></li>
                            <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'periodoncia']) }}">Periodoncia</a></li>
                            <li class="list-group-item"><a href="{{ route('servicios.url', ['url' => 'odontopediatria']) }}">Odontopediatría</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="contactanos" tabindex="-1" aria-labelledby="modalContacto" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-primary" id="modalContacto">Contáctanos</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <form>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <input type="text" name="nombre" class="form-control" id="nombre" aria-describedby="nombreText" required>
                        </div>
                        <div class="form-group">
                            <label for="telefono">Telefono (Whatsapp)</label>
                            <input type="text" name="telefono" class="form-control" id="telefono" aria-describedby="telefonoText" required>
                        </div>
                        <div class="form-group">
                            <label for="correo">Correo</label>
                            <input type="email" name="correo" class="form-control" id="correo" aria-describedby="correoText">
                        </div>
                        <div class="form-group">
                            <label for="detalle">Detalle del servicio que desea realizarse</label>
                            <textarea class="form-control" name="detalle" id="detalle" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary">ENVIAR</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

