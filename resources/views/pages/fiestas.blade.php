<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="Webflow" name="generator">
    <title>Zandona - Felices Fiestas</title>
    <link href="{{ asset('images/favicon.ico') }}" rel="shortcut icon" type="image/x-icon">
    <!-- styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Pacifico&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
</head>
<body class="bg-dark">
    {{-- <audio autoplay controls>
        <source src="{{ asset('images/merry-christmas.mp3') }}" type="audio/mpeg">
    </audio> --}}
    <audio id="audioID" loop><source src="{{ asset('images/merry-christmas.mp3') }}"  type="audio/mp3"></audio>
    <div id="app">

        <div class="loader-container">
            <div class="loader"></div>
        </div>

            <div class="container">
                <div class="row my-5">
                    <div class="col-md-6 offset-md-6 mx-auto text-center">

                        <div class="form animate__animated animate__faster" id="formNavidad">
                            <img src="{{ asset('images/zandona-navidenho.svg') }}" alt="" width="200">
                            <form name="navidad">
                                <div class="form-group my-4">
                                    <input id="inputNombre" class="form-control form-control-lg my-4" type="text" placeholder="NOMBRE" required>
                                    <button class="btn btn-outline-light btn-block">ENVIAR</button>
                                </div>
                            </form>
                        </div>

                        <div id="regalo" class="regalo">
                            <img src="{{ asset('images/zandona-fondo-navidenho.png') }}" class="img-fluid" alt="">
                            <div class="nombre-navidad"><span id="nombreNavidad">nombre</span></div>
                        </div>

                        <div id="compartir" class="mt-5 pt-4 d-none animate__animated"><p class="text-light">No te olvides de capturar y compartir en tus redes &#128248</p></div>

                    </div>
                </div>
            </div>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('lib/aos/aos.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/main.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/navidad.js') }}" type="text/javascript"></script>

</body>
</html>
