@extends('pages.base')

@section('banner')
<div class="row">
    <div class="col-12 col-md-5 col-lg-6 pt-5">
        <h2 class="text-primary" data-aos="fade-in" data-aos-delay="700" data-aos-easing="ease-in-out">La Clínica</h2>
        <p class="text-parrafo text-dark" data-aos="fade-in" data-aos-delay="1200" data-aos-easing="ease-in-out">Un lugar en el que cada detalle es parte esencial para su bienestar.</p>
    </div>
</div>
@endsection

@section('content')
    <section class="bg-dark">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-6 p-0 border border-dark"><img src="{{ asset('images/index/foto-clinica-2.jpg') }}" alt="foto de la clinica" class="img-fluid"></div>
                <div class="col-12 col-md-6 p-0 border border-dark"><img src="{{ asset('images/index/foto-clinica-3.jpg') }}" alt="foto del loby" class="img-fluid"></div>
            </div>
        </div>
    </section>

    <section class="bg-dark">
        <div class="container">
            <div class="row py-5">
                <div class="col-12">
                    <h3 class="text-primary">Perfecto equilibrio entre estructura y naturaleza</h3>
                    <p class="text-light mb-1">Tanto el interior como el exterior fueron meticulosamente diseñados con ciencia Ética y Estética para un perfecto equilibrio entre estructura y naturaleza.</p>
                    <p class="text-light">Los consultorios tienen  un enfoque de Eficiencia, Eficacia y Bioseguridad para los pacientes y profesionales, cumpliendo los estándares establecidos por el Ministerio de Salud y las exigencias municipales. Además nos regimos por el protocolo Internacional de Bioseguridad, así como también un compromiso cabal de mejora continua por un Sistema de Gestión de Calidad en Salud.</p>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 p-0"><img src="{{ asset('images/index/foto-clinica-4.jpg') }}" alt="foto de la clinica" class="img-fluid"></div>
            </div>
        </div>
    </section>

@endsection

@section('script')
<script>
    $('#banner').addClass("background-clinica background-banner");
</script>
@endsection
