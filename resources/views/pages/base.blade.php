<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="Webflow" name="generator">
    <title>@yield('title')</title>
    <link href="{{ asset('images/favicon.ico') }}" rel="shortcut icon" type="image/x-icon">
    <!-- styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/aos/aos.css') }}" rel="stylesheet">
    @yield('style')
</head>
<body>
    <div id="app">
        <div class="loader-container">
            <div class="loader"></div>
        </div>

        <header id="home">
            <div class="navbar-area bg-dark">
                <nav class="navbar navbar-expand-lg navbar-dark bg-dark w-100 fixed-top" id="navbar">
                    <div class="container">
                        <a class="navbar-brand svg-inverse" href="{{ route('pages.index') }}"><img src="{{ asset('images/icons/logo.svg') }}" height="45" alt="logo de la empresa"></a>

                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                                </button>
                        <div class="collapse navbar-collapse" id="navbarNavDropdown">
                            <ul class="navbar-nav w-100 justify-content-end">
                                <li class="nav-item">
                                    <a class="nav-link {{ request()->routeIs('pages.index') ? 'active' : '' }}" href="{{ route('pages.index') }}">Inicio</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ request()->routeIs('pages.profesionales') ? 'active' : '' }}" href="{{ route('pages.profesionales') }}">Profesionales</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ request()->routeIs('servicios.index') ? 'active' : '' }}" href="{{ route('servicios.index') }}">Servicios</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="https://zandonaodontologia.tumblr.com/">Blog</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ request()->routeIs('pages.contacto') ? 'active' : '' }}" href="{{ route('pages.contacto') }}" href="{{ route('pages.contacto') }}">Contacto</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>

            <div id="banner" class="banner">
                <div class="container">
                    @yield('banner')
                </div>
            </div>

        </header>

        @yield('content')

        <footer class="bg-dark">
            <div class="container">
                <div class="row py-4 py-md-5 align-items-center text-lg-center">

                    <div class="col-md-3 text-center text-md-left">
                        <img src="{{ asset('images/icons/logo.svg') }}" alt="" width="150" class="svg-inverse mb-3" data-aos="fade-in"  data-aos-delay="300">
                    </div>
                    <div class="col-md-9 text-center text-md-right" data-aos="fade-in"  data-aos-delay="300">
                        <p class="text-white m-0">San Martín 2114 casi Molas López</p>
                        <p class="text-light m-0"> 021 605 301 | 0981 408 793</p>
                        <div class="my-2">
                            <a href="https://www.facebook.com/zandonaclinicaodontologica/"><img src="{{ asset('images/icons/icon-facebook.svg') }}" alt="icono de facebook" width="25" class="mr-2" data-aos="fade-up"  data-aos-delay="200" data-aos-duration="1000"></a>
                            <a href="https://www.instagram.com/zandonaodontologiaoficial/"><img src="{{ asset('images/icons/icon-instagram.svg') }}" alt="icono de instagram" width="25" class="mr-0" data-aos="fade-up"  data-aos-delay="200" data-aos-duration="1000"></a>
                        </div>
                    </div>
                </div>
                <div class="row border-top text-center">
                    <div class="col-12">
                        <p class="text-center text-light my-3">{{  now()->year }}©. Derechos Reservados. - Desarrollado por: <a href="http://www.puntocode.com.py">PUNTOCODE</a> </p>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('lib/aos/aos.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/main.js') }}" type="text/javascript"></script>
    @yield('script')
</body>
</html>
