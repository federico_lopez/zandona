@extends('pages.base')

@section('banner')
<div class="row mision align-items-center fullHeight">
    <div class="col-12 col-md-8 col-lg-10">
        <h2 class="text-primary"  data-aos="fade-in" data-aos-delay="700" data-aos-easing="ease-in-out">Misión</h2>
        <p class="text-light py-2 mb-3 text-parrafo"  data-aos="fade-in" data-aos-delay="1200" data-aos-easing="ease-in-out">La Misión de la Clínica Zandona es la devolución de la <span class="font-weight-bold">salud, función y Estética</span> de los dientes, con conocimientos y materiales científicamente comprobados para la preservación de lo mismos a lo largo de la vida, con Profesionales éticamente comprometidos.</p>
    </div>
</div>
@endsection

@section('script')
<script>
    $('#banner').addClass("background-mision background-banner");
</script>
@endsection
