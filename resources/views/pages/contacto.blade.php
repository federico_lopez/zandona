@extends('pages.base')

@section('banner')
<div class="row text-center">
    <div class="col-12" data-aos="fade-in"  data-aos-easing="ease-in-out">
        <h2 class="text-uppercase">Contacto</h2>
        <p data-aos="fade-in" data-aos-delay="600" data-aos-easing="ease-in-out"><a href="{{ route('pages.index') }}">Inicio</a></p>
    </div>
</div>
@endsection

@section('content')

    <section class="my-5">
        <div class="container">
            @if (session('flash'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('flash') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            <div class="row">

                <div class="col-md-8 pr-md-5">
                    <h4 class="mb-0 text-uppercase font-weight-bold text-primary-dark">Envíanos un mensaje</h4>
                    <p class="font-italic">Intentaremos responderte en la brevedad posible.</p>
                    @include('layouts.form-contact')
                </div>

                <div class="col-md-4 pt-5 pt-md-0">
                    <h3>Contáctanos</h3>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a href="https://wa.link/db6ssf" class="nav-link"><img src="{{ asset('images/icons/whatsapp.svg') }}" alt="icono de whatsapp" height="20" class="pr-2">0981 408793</a>
                        </li>
                        <li class="nav-item">
                            <a href="mailto:cozandona@gmail.com" class="nav-link"><img src="{{ asset('images/icons/correo.svg') }}" alt="" height="20" class="pr-2">cozandona@gmail.com</a>
                        </li>
                        <li class="nav-item">
                            <p class="text-primary pt-2"><img src="{{ asset('images/icons/mapa.svg') }}" alt="" height="20" class="pr-2">San Martín 2114 casi Molas López</p>
                        </li>
                        <li class="nav-item pt-2">
                            <a href="https://g.page/Zandonaodontologia?share" class="btn btn-outline-primary btn-block"><img src="{{ asset('images/icons/icon-map-marker.svg') }}" alt="" height="20"> Ver en el Mapa</a>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </section>

@endsection

@section('script')
<script>
    $('#banner').addClass("background-contacto");
</script>
@endsection
