@extends('pages.base')

@section('content')
    <section id="banner-profesionales" class="mt-5">
        <div class="container-fluid p-0">
            <img src="{{ asset('images/profesionales/banner.jpg') }}" alt="banner de profesionales" class="img-profesionales">
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row pt-5 pb-3">
                <div class="col-sm-8 mx-auto text-center">
                    <h2 class="text-primary">Nuestros Profesionales</h2>
                    <p>Las Personas que integran nuestro equipo son Profesionales exclusivos, con mucha experiencia, y mucha calidad humuna para brindarte la mejor atención.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="mb-5 pb-3">
        <div class="container">
            <div class="row">
                <div class="col-md-4 mb-4">
                    <div class="card">
                        <img class="card-img-top" src="{{ asset('images/profesionales/foto-dra-sandra.png') }}" alt="foto de la Dra. Zandona">
                        <div class="card-body">
                          <h3 class="card-title">Dra. Sandra Zandona</h3>
                          <p class="card-text text-muted">Especialista en Dentística por la Asociación Brasilera de Odontología.</p>
                        <a href="{{ route('profesionales.url', ['url' => 'sandra']) }}" class="btn btn-outline-dark btn-block">Ver CV</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card mb-4">
                        <img class="card-img-top" src="{{ asset('images/profesionales/foto-dr-miguel.png') }}" alt="foto del Dr. Riquelme">
                        <div class="card-body">
                          <h3 class="card-title">Dr. Miguel Riquelme Rodas</h3>
                          <p class="card-text">Master en Biología Oral con énfasis en Implantes.</p>
                          <a href="{{ route('profesionales.url', ['url' => 'miguel']) }}" class="btn btn-outline-dark btn-block">Ver CV</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card mb-4">
                        <img class="card-img-top" src="{{ asset('images/profesionales/foto-dra-silvia.png') }}" alt="foto de la Dra. Silvia">
                        <div class="card-body">
                          <h3 class="card-title">Dra. Silvia Zelada</h3>
                          <p class="card-text">Especialista en Ortodoncia y Ortopedia Funcional.</p>
                          <a href="{{ route('profesionales.url', ['url' => 'silvia']) }}" class="btn btn-outline-dark btn-block">Ver CV</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card mb-4">
                        <img class="card-img-top" src="{{ asset('images/profesionales/foto-dra-noelia.png') }}" alt="foto de la Dra. Noela">
                        <div class="card-body">
                          <h3 class="card-title">Dra. Noelia Olmedo</h3>
                          <p class="card-text">Especialista en Odontopediatría, Ortodoncia, Clínica General.</p>
                          <a href="{{ route('profesionales.url', ['url' => 'noelia']) }}" class="btn btn-outline-dark btn-block">Ver CV</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card mb-3">
                        <img class="card-img-top" src="{{ asset('images/profesionales/foto-dr-carlos.png') }}" alt="foto del Dr. Carlos">
                        <div class="card-body">
                          <h3 class="card-title">Dr. Carlos Heilborn</h3>
                          <p class="card-text">Especialista en Endodoncia.</p>
                          <a href="{{ route('profesionales.url', ['url' => 'carlos']) }}" class="btn btn-outline-dark btn-block">Ver CV</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection



@section('script')
<script>
    $('#banner').remove();
</script>
@endsection
