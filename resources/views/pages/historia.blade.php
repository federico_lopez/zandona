@extends('pages.base')

@section('banner')
<div class="row pb-5">
    <div class="col-12 col-sm-8 offset-sm-4 col-md-6 offset-md-6 color-historia">
        <h2 class="text-primary text-sm-right"  data-aos="fade-in" data-aos-delay="700" data-aos-easing="ease-in-out">Cuándo empieza uno a construir un sueño?</h2>
        <p class="pt-2 text-sm-right text-dark"  data-aos="fade-in" data-aos-delay="1200" data-aos-easing="ease-in-out">
            Hoy les quiero compartir parte de mi historia, que es la historia de mi emprendimiento, <span class="font-weight-bold">Zandoná</span> que está próxima a celebrar sus 25 años. Me gustaría compartirles la historia que dio inicio a este sueño pero que comenzó a gestarse mucho tiempo antes.</p>
        <p class="text-sm-right pb-3"  data-aos="fade-in" data-aos-delay="1200" data-aos-easing="ease-in-out">A mí me gustan las historias. Soy una persona muy curiosa y tengo un interés genuino por conocer de dónde vienen los objetos que me gustan, los servicios a los que voy, lo que elijo comprar...</p>
    </div>
</div>
@endsection

@section('content')
<section id="historia-lampara">
    <div class="container px-4">
        <div class="row py-5" data-aos="fade-in" data-aos-easing="ease-in-out" data-aos-delay="1500">
            <div class="col-12 col-sm-9 col-md-8 col-lg-7">
                <p>Yo fui una hija de abuela. Mi padre falleció cuando yo era muy chica y mi madre, una joven viuda, tuvo que separar a sus cuatro hijos para darles una mejor calidad de vida. Así que muy pequeña me fui a vivir con mi abuela, pero con mi madre muy presente en la distancia. </p>
                <p>Con ella aprendí a hacer manualidades  pintaba al óleo tejía a dos agujas, bordaba blanquerías  y  mis ocios se dividían  recetas de cocina de la abuela, clases de baile y lecturas simple más continua.</p>
                <p>Con ella también aprendí la importancia de fijarme en los detalles, aunque sean los más mínimos, esa atención y cuidado al más mínimo detalle hace la diferencia siempre entre algo bueno y algo excelente.</p>
                <p>A mí siempre me gustó que las cosas fueran lo mejor que puedan ser, que sean excelentes.</p>
            </div>
        </div>
    </div>
</section>

<section id="historia-reloj">
    <div class="container px-4">
        <div class="row py-5" data-aos="fade-in" data-aos-easing="ease-in-out">
            <div class="col-12 col-sm-10 offset-sm-2 col-md-8 offset-md-4">
                <p class="text-sm-right">Mi abuela influenció mi gusto por la <span class="font-weight-bold">Estética</span>, la <span class="font-weight-bold">Armonía</span>, y la <span class="font-weight-bold">Perfección</span>. De adulta entendí que esa belleza sólo se puede construir con <span class="font-weight-bold">Ética e Integridad</span>.</p>
                <p class="text-sm-right">Mi madre en la distancia me dio grandes lecciones de trabajo arduo. Ella es una gran trabajadora que enfrentó la vida y sus adversidades haciendo lo mejor que podía hacer, trabajando.</p>
                <p class="text-sm-right">Ellas han sido parte vital de mis decisiones de vida. Sus lecciones de Estética y Ética marcaron mi camino profesional, no solo marcaron el qué hacer, sino sobre todo, cómo hacerlo.</p>
                <p class="text-sm-right">Y esas dos virtudes: la pasión por el detalle y la entrega al trabajo arduo sin tregua son los principales pilares del inicio de mi gran sueño: una <span class="font-weight-bold">Clínica de Odontología Especializada en crear Sonrisas Perfectas</span>, con todo lo que implica lograr eso.</p>
            </div>
        </div>
    </div>
</section>

<section id="historia-tarjeta">
    <div class="container px-4">
        <div class="row py-5" data-aos="fade-in" data-aos-easing="ease-in-out">
            <div class="col-12 col-sm-9 col-md-8 col-lg-7">
                <p>Me gradue muy joven y a los 23 años inauguré mi primer consultorio en el centro de Asunción. Un primer paso logrado con bastante esfuerzo, con los ahorros trabajados de domingo a domingo, sin parar, incluso haciendo largos viajes al interior para llegar a tiempo y atender a muchas personas que confiaban en mí.</p>
                <p>Mirando hacia atrás y viendo los 25 años que han pasado desde entonces. Veo los legados de mi abuela y mi madre en cada cosa que he hecho, en cada decisión que he tomado. Esas pasiones, por cada mínimo detalle y el trabajo continuo es lo que quiero transmitir a mis pacientes y seguir consolidando en mi Clínica, haciendo de la experiencia <span class="font-weight-bold">Zandoná</span>, una experiencia médica integral diferente, donde encontrarán a una persona y su equipo que estás 200% comprometidos con ayudarle a mejorar su salud bucal, ofreciéndole no solo buenas experiencias, sino excelentes y sostenible.</p>
                <p>Esa es la historia de mi sueño. Un servicio que hoy está consolidado y orgulloso de que ustedes sean parte de su historia. Una historia que comenzó a escribirse hace muchos años y seguirá ofreciendo a quienes confían en ella,  <span class="font-weight-bold">Excelencia, la Experiencia y Compromiso</span> porque eso, es lo que yo aprendí a hacer.</p>
            </div>
        </div>
    </div>
</section>
@endsection

@section('script')
<script>
    $( "#banner" ).addClass( "background-banner background-historia" );
</script>
@endsection
