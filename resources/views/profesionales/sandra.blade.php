@extends('pages.base')

@section('banner')
<div class="row text-center">
    <div class="col-12" data-aos="fade-in"  data-aos-easing="ease-in-out">
        <h2 class="text-light text-uppercase">Profesionales</h2>
        <p data-aos="fade-in" data-aos-delay="600" data-aos-easing="ease-in-out"><a href="{{ route('pages.profesionales') }}">Volver</a></p>
    </div>
</div>
@endsection

@section('content')

    <section class="my-5">
        <div class="container">
            <div class="row">

                <div class="col-md-4">
                    <img class="card-img-top" src="{{ asset('images/profesionales/foto-dra-sandra.png') }}" alt="foto de la Dra. Sandra">
                </div>

                <div class="col-md-8">
                    <h3 class="pt-4 pt-md-0 mb-0">Dra. Sandra Zandona</h3>
                    <p class="text-primary">Odontóloga - Empresaria</p>
                    <h4>Rol Empresarial</h4>
                    <p>Fundador, director y accionista:</p>
                    <ul>
                        <li>Zandoná Odontología Especializada<br>Asunción Py.</li>
                    </ul>
                    <p>Accionista:</p>
                    <ul>
                        <li>Salto Qualitativo  Consultoria e Assessoria Empresarial - Brasil.</li>
                        <li>Clinidermn - Brasil</li>
                    </ul>
                    <p>Miembro Activo:</p>
                    <ul>
                        <li>Club de Ejecutivos del Paraguay </li>
                        <li>Federación Internacional de Coaching Ontológico - Ficop</li>
                        <li>Asociación Brasilera de Odontología</li>
                        <li>Circulo de Odontólogos del Paraguay</li>
                    </ul>
                    <p>Experiencia Profesional:</p>
                    <ul>
                        <li>Experiencia Clínica en la Armada Nacional (1995-2003)</li>
                        <li>Consultoría en Control de Calidad en Clínicas Odontológica - Brasil</li>
                        <li>Director Ejecutivo de Centopeia Calzados Importaciones</li>
                        <li>Director Ejecutivo de Recinto Doña Antonia Restaurant</li>
                    </ul>
                    <h4>Bagaje Académico:</h4>
                    <ul>
                        <li>Egresada de la UN Facultad de Odontología</li>
                        <li>Especialista en Dentística por la Asociación Brasilera de Odontología</li>
                        <li>Pos graduación en Rehabilitación Estética Oral - Brasil</li>
                        <li>Pos graduación en Planeamiento Rehabilitador Diseño de Sonrisa Brasil</li>
                        <li>Pos graduación en Cirugías de Terceros Molares Incluidos - Brasil</li>
                        <li>Pos graduación en Cirugía Plástica de la Encía - Brasil</li>
                        <li>Especialización en Implantes Dentales - Brasil</li>
                        <li>Cursos y Conferencias  en Argentina - Uruguay - EE.UU - España - Brasil</li>
                        <li>Participación anual -  Congreso Internacional de Odontología (1995 - 2020) Sao Paulo</li>
                        <li>Consultoría en Gestión de Clínica en alianza con Salto Cualitativo</li>
                        <li>Coach Ontológico - Crear Integración Consultores</li>
                        <li>Capacitación de empreendedorismo certificado  por PNUD/ONU</li>
                        <li>Programación Neurolingüística Nivel 1 - Nivel 2 - Nivel 3</li>
                    </ul>
                </div>

            </div>
        </div>
    </section>

@endsection



@section('script')
<script>
    $('#banner').addClass("background-profesionales background-banner");
</script>
@endsection
