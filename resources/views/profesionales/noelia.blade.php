@extends('pages.base')

@section('banner')
<div class="row text-center">
    <div class="col-12" data-aos="fade-in"  data-aos-easing="ease-in-out">
        <h2 class="text-light text-uppercase">Profesionales</h2>
        <p data-aos="fade-in" data-aos-delay="600" data-aos-easing="ease-in-out"><a href="{{ route('pages.profesionales') }}">Volver</a></p>
    </div>
</div>
@endsection

@section('content')

    <section class="my-5">
        <div class="container">
            <div class="row">

                <div class="col-md-4">
                    <img class="card-img-top" src="{{ asset('images/profesionales/foto-dra-noelia.png') }}" alt="foto de la Dra. Noelia">
                </div>

                <div class="col-md-8">
                    <h3 class="pt-4 pt-md-0 mb-0">Dra. Noelia Olmedo</h3>
                    <p class="text-primary">Egresada de la UNA</p>
                    <p>Ortodoncista, Clinica General y Odontopediatría.</p>
                    <p>Posgraduación en Odontopediatria - <span class="font-lighter">A.B.O.</span></p>
                    <p>Ortodoncia <span class="font-weight-lighter">M.T.B. - C.O.P.</span></p>
                    <p>Capacitacioón en Odontologia Preventiva y Ortopedia - <span class="font-weight-lighter">I.C.D.E.</span></p>
                    <p>Capacitación en Ortodoncia Lingual y ATM de los maxilares - <span class="font-weight-lighter">C.O.P.</span></p>
                    <p>Posgraduación en Ortodoncia y Ortopedia Facial de los Maxilares Transdiciplina - <span class="font-weight-lighter">I.C.D.E.</span></p>
                </div>

            </div>
        </div>
    </section>

@endsection



@section('script')
<script>
    $('#banner').addClass("background-profesionales background-banner");
</script>
@endsection
