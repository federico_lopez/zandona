@extends('pages.base')

@section('banner')
<div class="row text-center">
    <div class="col-12" data-aos="fade-in"  data-aos-easing="ease-in-out">
        <h2 class="text-light text-uppercase">Profesionales</h2>
        <p data-aos="fade-in" data-aos-delay="600" data-aos-easing="ease-in-out"><a href="{{ route('pages.profesionales') }}">Volver</a></p>
    </div>
</div>
@endsection

@section('content')

    <section class="my-5">
        <div class="container">
            <div class="row">

                <div class="col-md-4">
                    <img class="card-img-top" src="{{ asset('images/profesionales/foto-dra-silvia.png') }}" alt="foto de la Dra. Silvia">
                </div>

                <div class="col-md-8">
                    <h3 class="pt-4 pt-md-0 mb-0">Dra. Silvia Zelada</h3>
                    <p class="text-primary">Egresada de la UNA</p>
                    <p>Especialización en Ortodoncia y Ortopedia Funcional en la <span class="font-italic">Facultad de Odontología de Bauru - USP</span>.</p>
                    <p>Especialización en Ortodoncia en la <span class="font-italic">UAP - Pierre Fauchard</span>.</p>
                    <p>Especialización en Ortopedia Funcional en la <span class="font-italic">UAP - Pierre Fauchard</span>.</p>
                    <p>Auxiliar de Enseñanza Cátedra de Ortodoncia I, 4° año UNA.</p>
                </div>

            </div>
        </div>
    </section>

@endsection



@section('script')
<script>
    $('#banner').addClass("background-profesionales background-banner");
</script>
@endsection
