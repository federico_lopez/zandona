@extends('pages.base')

@section('banner')
<div class="row text-center">
    <div class="col-12" data-aos="fade-in"  data-aos-easing="ease-in-out">
        <h2 class="text-light text-uppercase">Profesionales</h2>
        <p data-aos="fade-in" data-aos-delay="600" data-aos-easing="ease-in-out"><a href="{{ route('pages.profesionales') }}">Volver</a></p>
    </div>
</div>
@endsection

@section('content')

    <section class="my-5">
        <div class="container">
            <div class="row">

                <div class="col-md-4">
                    <img class="card-img-top" src="{{ asset('images/profesionales/foto-dr-miguel.png') }}" alt="foto del Dr. Miguel">
                </div>

                <div class="col-md-8">
                    <h3 class="pt-4 pt-md-0 mb-0">Dr. Miguel Riquelme Rodas</h3>
                    <p class="text-primary">Egresado de la UNA</p>
                    <p>Máster en Biología Oral con énfasis en Implantes USC.</p>
                    <p>Especialista en Periodoncia por la USP.</p>
                    <p>Especialista en didáctica Universitaria UAP.</p>
                    <p>Especialista en Implantes por Uninga.</p>
                </div>

            </div>
        </div>
    </section>

@endsection



@section('script')
<script>
    $('#banner').addClass("background-profesionales background-banner");
</script>
@endsection
