@extends('pages.base')

@section('banner')
<div class="row text-center">
    <div class="col-12" data-aos="fade-in"  data-aos-easing="ease-in-out">
        <h2 class="text-light text-uppercase">Profesionales</h2>
        <p data-aos="fade-in" data-aos-delay="600" data-aos-easing="ease-in-out"><a href="{{ route('pages.profesionales') }}">Volver</a></p>
    </div>
</div>
@endsection

@section('content')

    <section class="my-5">
        <div class="container">
            <div class="row">

                <div class="col-md-4">
                    <img class="card-img-top" src="{{ asset('images/profesionales/foto-dr-carlos.png') }}" alt="foto del Dr. Carlos">
                </div>

                <div class="col-md-8">
                    <h3 class="pt-4 pt-md-0 mb-0">Dr. Carlos Heilborn</h3>
                    <p class="text-primary">Egresado de la UNA</p>
                    <p>Especialización en Endodoncia <br><span class="font-italic">Universidad de Concepción - Chile</span>.</p>
                    <p>Profesor visitante departamento de Endodoncia <br><span class="font-italic">Universidad de Washington - USA</span>.</p>
                    <p>Miembro asociado de la American Association of Endodontists <span class="font-weight-lighter">(AAE)</span>.</p>
                    <p>Past Presidente de la Asociación Paraguaya de Endodoncia.</p>
                    <p>Coordinador del curso de Especialización en Endodoncia del IOA.</p>
                </div>

            </div>
        </div>
    </section>

@endsection



@section('script')
<script>
    $('#banner').addClass("background-profesionales background-banner");
</script>
@endsection
