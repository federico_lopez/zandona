const divRegalo = document.querySelector('#regalo');
const formNavidad = document.querySelector('#formNavidad');
const nombreNavidad = document.querySelector('#nombreNavidad');
const inputNombre = document.querySelector('#inputNombre');
const divCompartir = document.querySelector('#compartir');
const myaudio = document.querySelector("#audioID");


eventListeners();

function eventListeners() {
    formNavidad.addEventListener('submit', ponerNombre);
}

function ponerNombre(e) {
    e.preventDefault();
    //console.log(inputNombre.value);
    if (inputNombre.value.length > 10) {
        alert('El nombre debe tener menos de 10 caracteres');
        return;
    }

    myaudio.play();
    nombreNavidad.textContent = inputNombre.value;
    formNavidad.style.opacity = '0';
    setTimeout(function() {
        formNavidad.classList.add('d-none');
        divRegalo.classList.add('fadeIn');
        divCompartir.classList.remove('d-none');
    }, 1000);

    setTimeout(() => {
        divCompartir.classList.add('animate__fadeOut');
    }, 4000);

}