<?php

namespace App\Http\Controllers;

use App\Mail\MessageReceived;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;

class PagesController extends Controller
{
    public function index(){
        return view('pages.index');
    }
    public function mision(){
        return view('pages.mision');
    }
    public function historia(){
        return view('pages.historia');
    }
    public function profesionales(){
        return view('pages.profesionales');
    }
    public function clinica(){
        return view('pages.clinica');
    }
    public function servicios(){
        return view('servicios.index');
    }
    public function contacto(){
        return view('pages.contacto');
    }
    public function fiestas(){
        return view('pages.fiestas');
    }

    public function contactoMail(){
        $data = request()->validate([
            'nombre' => 'required',
            'email' => 'required',
            'servicio' => 'required',
            'doctor' => 'required',
            'mensaje' => 'required',
        ]);

        Mail::to('puntocodepy@gmail.com')->queue(new MessageReceived($data));
        //Mail::to('fede.xavier@outlook.com')->send(new MessageReceived($data));
        //return new MessageReceived($data);
        return back()->with('flash', 'La consulta ha sido enviada correctamente');
    }

}
