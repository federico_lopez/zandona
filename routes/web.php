<?php

use App\Http\Controllers\PagesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PagesController::class, 'index'])->name('pages.index');
Route::get('/mision', [PagesController::class, 'mision'])->name('pages.mision');
Route::get('/historia', [PagesController::class, 'historia'])->name('pages.historia');
Route::get('/profesionales', [PagesController::class, 'profesionales'])->name('pages.profesionales');
Route::get('/clinica', [PagesController::class, 'clinica'])->name('pages.clinica');
Route::get('/servicios', [PagesController::class, 'servicios'])->name('servicios.index');
Route::get('/contacto', [PagesController::class, 'contacto'])->name('pages.contacto');
Route::post('/contacto-mail', [PagesController::class, 'contactoMail'])->name('pages.mail');
Route::get('/fiestas', [PagesController::class, 'fiestas'])->name('pages.fiestas');

Route::get('/profesionales/{url}', function($url){
    return view()->first(['profesionales.'.$url]);
})->name('profesionales.url');

Route::get('/servicios/{url}', function($url){
    return view()->first(['servicios.'.$url]);
})->name('servicios.url');

Auth::routes();

